APB Name
=========

SSO Manager APB will allow users to register their sites under SSO.

APB Variables
--------------

Variables depend on the chosen plan, but normally they are:

- `_apb_plan_id`: shibboleth | saml2 | saml2mellon | saml2online
- `namespace`: the namespace
- `sso_api_user`: Username for authentication
- `sso_api_pwd`: Password for authentication

For SAML2Mellon and SAML2Online, the following variables are also needed:

- `metadata_uri`: https://myapp.web.cern.ch/myfolder/mymetadata (For the moment only allows absolute metadata url).
- `basic_authz`: CERN Registered,CERN Shared, ...

Sample of using variables

```
provision
--extra-vars '{"_apb_plan_id":"saml2online","namespace":"test-d8-apb","sso_api_user":"xxx","sso_api_pwd":"xxx", metadata_uri":"https://test-d8-apb.web.cern.ch/simplesaml/my-metadata","basic_authz":"CERN Registered,CERN Shared,HEP Trusted,Verified External,Unverified External"}'
```

Author Information
------------------

Drupal Admins in collaboration with Openshift Admins.
